import java.util.ArrayList;
import java.util.List;

public class Grupa implements Element {
    private final String data;
    private Element parent;
    private final List<Element> children;

    public Grupa(String data) {
        this.data = data;
        children = new ArrayList<>();
    }

    public void setParent(Element parent) {
        this.parent = parent;
    }

    @Override
    public Element add(Element element) {
        if (element instanceof Semigrupa) {
            children.add(element);
            element.setParent(this);
            return element;
        } else {
            children.get(children.size()-1).add(element);
        }
        //System.out.println("NULL in Grupa");
        return parent;
    }

    @Override
    public void accept(PrintVisitor visitor) {
        visitor.execute("\t" + data + "\n");
        for (Element child : children) {
            child.accept(visitor);
        }
    }
}
