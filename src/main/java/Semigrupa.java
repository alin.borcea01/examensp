import java.util.ArrayList;
import java.util.List;

public class Semigrupa implements Element {
    private final String data;
    private Element parent;
    private final List<Element> children;

    public Semigrupa(String data) {
        this.data = data;
        children = new ArrayList<>();
    }

    @Override
    public void accept(PrintVisitor visitor) {
        visitor.execute("\t\t" + data + "\n");
        for (Element child : children) {
            child.accept(visitor);
        }
    }

    @Override
    public void setParent(Element element) {
        parent = element;
    }

    @Override
    public Element add(Element element) {
        if (element instanceof Student) {
            children.add(element);
            element.setParent(this);
            return this;
        } else if (element instanceof Semigrupa) {
            return parent.add(element);
        } else if (element instanceof Grupa) {
            return parent.add(element);
        }

        //System.out.println("NULL in Semi");
        return parent;
    }
}
