import java.util.ArrayList;
import java.util.List;

public class An implements Element {
    private final String data;
    private Element parent = null;
    private final List<Element> children;

    public An(String data) {
        this.data = data;
        children = new ArrayList<>();
    }

    @Override
    public Element add(Element element) {
        if (element instanceof Grupa) {
            element.setParent(this);
            children.add(element);
        } else {
            children.get(children.size()-1).add(element);
        }
        return this;
    }

    public void setParent(Element element) {
        parent = null;
    }

    @Override
    public void accept(PrintVisitor visitor) {
        visitor.execute(data + "\n");
        for (Element child : children) {
            child.accept(visitor);
        }
    }
}
