public interface Element {
    void accept(PrintVisitor visitor);

    void setParent(Element element);

    Element add(Element element);
}
