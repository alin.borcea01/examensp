public class Student implements Element {
    private final String name;
    private final String mail;
    private Element parent = null;

    public Student(String name, String mail) {
        this.name = name;
        this.mail = mail;
    }

    @Override
    public void accept(PrintVisitor visitor) {
        visitor.execute("\t\t\t\t" + name + ", " + mail + "\n");
    }

    @Override
    public void setParent(Element element) {
        parent = element;
    }

    @Override
    public Element add(Element element) {
        return parent.add(element);
    }
}
